# The problem

As I interpreted it, the problem is about wrapping the github API, specifically
this call:

`curl https://api.github.com/search/users?q=location:barcelona+sort:repositories\&page=1\&per_page=50`

Note that this will also return organizations, not just individual people. The
requirements were not clear about that, but I did not think it was important.

Furthermore, it looks like this API does not understand the notion of city. I
did not do anything about this, keeping the keyword 'location' as a
parameter. If we really want our service to only accept cities, we would first
need to keep a registry of all cities, then try to map them to github
locations; this is tricky since some users write their locations differently,
e.g. 'sanfrancisco' (no space) and 'San Francisco'. I did not bother dealing
with this issue, but if it were a real product this would bug me.

I tried to keep things simple, but given the open nature of this problem it is
hard to know when to stop developping: you don't want to deliver something too
basic, yet at the same time it is impossible to do something that would be
production ready. I hope I hit the right balance.

# Installation

To install this software, you need to have git, python3 and pip3 installed on
your machine.

First, get the repository:

`git clone https://bitbucket.org/julienlegoff/nr.git`

Then, enter the directory and do:

`pip3 install -r requirements.txt`

Then:

`./run.sh`

The service should be launched.

# Testing

To test if the service is working, do:

`curl -G localhost:5000/top -d "n=50" -d "location=barcelona"`

This should return something like:

`{"users": ["ajsb85", "leobcn", "ardock", "nilportugues", "joeromero", "pataquets", "netmarti", "Vizzuality", "jmcarbo", "tcorral", "areski", "mikz", "VictorBjelkholm", "rubiojr", "trikitrok", "albodelu", "klich3", "nilopc-interesting-libs", "juanmaguitar", "PlacetoBiz", "josephmartz", "iomedhealth", "EdZava", "project-open", "DavidVazGuijarro", "drpicox", "ivosandoval", "opsb", "pimterry", "soywiz", "lordofthejars", "sdesimone", "santihbc", "Softsapiens", "leplatrem", "weisk", "chrisekelley", "ConfusedReality", "asolfre", "ovicin", "PabloCastellano", "bicherele", "gonzaloserrano", "alxndrsn", "diasjorge", "kidd", "NTTCom-MS", "codegram", "skyrpex", "christkv"], "total_count": 50}`

## Authentication

I implemented a basic authentication scheme in a separate branch. I did not
merge the branch to master because you need a database in order to use it, and
I thought this might be cumbersome. If you want to try it though, do:

`git clone auth`

then install the extra requirements:

`pip3 install -r requirements.txt`

and set up the database:

`python3 create_database.py`

`python3 create_user.py user pass`

You can then launch the application again:

`./run.sh`

And test it:

`curl  -G localhost:5000/top -d "n=50" -d "location=barcelona"`

will return a 401 error code.

But:

`curl -u user:pass -G localhost:5000/top -d "n=50" -d "location=barcelona"`

will work.

I also implemented a basic token scheme. To use it, do:

`curl -u user:pass -XPOST localhost:5000/authentication`

This will return something like:

`{"token": "xxx"}`

Now, you can use the token to query the service:

`curl -H "Authorization: Token xxx" -G localhost:5000/top -d "n=50" -d "location=barcelona"`

The token will expire in 10 minutes.

# Implementation

I chose Flask as web framework, especially because of its simplicity.

I implemented a very basic textbook API call and added a few features. Each
commit in the repository corresponds to one of those features.

Since the API relies on an external service, I tried to be as gentle as
possible with it, and to keep the service running even if it is down. To do
this:

  * I added a 5 seconds timeout when connecting to the github api
  * I implemented (or rather, copied!) a simple in-memory cache
  * I wrapped the API call with a circuit-breaker: this way, if github goes
    down for a long period of time, our service won't even try to call it.

Obviously, those are very crude implementations. The cache and circuit breaker
both only work locally, so they will be useless in a multiple worker
environment. In practice, you would use a service like Redis to make this
work.

I thought of making the calls through a message queue, which would also be a
good idea in practice, but in my opinion it would be beyond the scope of the
exercise; it would also have made the installation of the software more
difficult.

The calls to github use the open API, which has a severe rate limit. In
practice, it would probably be a better idea to register you application to
github and use an access key when calling the API.
