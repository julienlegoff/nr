from circuitbreaker import circuit
from urllib.parse import urljoin
import requests


GITHUB_URL = None


class ServiceUnavailable(Exception):
    pass


@circuit
def get_top(location, n):
    params = {"sort": "repositories",
              "page": 1,
              "per_page": n,
              "q": "location:{}".format(location)}
    try:
        r = requests.get(urljoin(GITHUB_URL, "search/users"),
                         params=params, timeout=5)
    except requests.RequestException:
        raise ServiceUnavailable()

    if r.status_code != 200:
        raise ServiceUnavailable()

    items = r.json()["items"]
    return [it["login"] for it in items]
