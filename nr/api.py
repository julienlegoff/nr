from circuitbreaker import CircuitBreakerError
from flask import Flask, request
from flask_restful import abort, Api, Resource

from . import cache, github

app = Flask(__name__)
app.config.from_object('nr.config')
github.GITHUB_URL = app.config["GITHUB_URL"]

api = Api(app)


class Top(Resource):

    @cache.cached()
    def get(self):
        location = request.args.get("location")
        if not location:
            abort(400, error={"message": "Please provide a location"})
        n = request.args.get("n")
        if not n:
            abort(400, error={
                "message": "Please provide a value for n (50, 100 or 150)"})
        try:
            n = int(n)
        except ValueError:
            abort(400, error={
                "message": "n must be an integer"
            })
        if n not in (50, 100, 150):
            abort(400, error={
                "message": "n must be 50, 100 or 150"
            })

        try:
            items = github.get_top(location, n)
        except (github.ServiceUnavailable, CircuitBreakerError):
            abort(503, error={
                "message": "The service is temporarily unavailable."
            })

        results = {
            "total_count": len(items),
            "users": items
        }
        return results

api.add_resource(Top, '/top')

if __name__ == '__main__':
    app.run(debug=True)
